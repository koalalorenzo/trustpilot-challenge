module gitlab.com/koalalorenzo/trustpilot-challenge

go 1.15

require (
	github.com/sirupsen/logrus v1.7.0
	github.com/spf13/cobra v1.0.0
	github.com/stretchr/testify v1.2.2
	golang.org/x/sys v0.0.0-20201009025420-dfb3f7c4e634 // indirect
)
