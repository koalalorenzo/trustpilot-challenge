package main

import "gitlab.com/koalalorenzo/trustpilot-challenge/cmd"

func main() {
	cmd.Execute()
}
