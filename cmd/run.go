package cmd

import (
	"math/rand"
	"os"
	"runtime"
	"sort"
	"strings"
	"sync"
	"time"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"

	"gitlab.com/koalalorenzo/trustpilot-challenge/core"
)

var defaultHashes = []string{
	"e4820b45d2277f3844eac66c903e84be",
	"23170acc097c24edb98fc5488ab033fe",
	"665e5bcb0c20062fe8abaaf4628bb154",
	"4a9f51db2c7eba0c724499f749d3176a", // trustpilot wants you 😜
}

// runCmd represents the run command
var runCmd = &cobra.Command{
	Use:   "run [path]",
	Args:  cobra.ExactArgs(1),
	Short: "Search for possible combinations of words in dictionary ",
	Long:  `This will make different combinations in the dictionary provided [path] and compare them in with the md5 hashes provided`,
	Run:   runRun,
}

func init() {
	rootCmd.AddCommand(runCmd)
	runCmd.Flags().StringP("letters", "l", "poultryoutwitsants", "specifies the letters to use for the filter")
	runCmd.Flags().BoolP("shuffle", "s", false, "shuffle the dictionary before searching for combinations")
	runCmd.Flags().BoolP("async", "a", false, "spawns goroutine to find solutions. This use a lot of memory")
	runCmd.Flags().BoolP("exitasap", "e", false, "exit as soon as any phrase has been found")
	runCmd.Flags().BoolP("debug", "d", false, "Show debugging logs")
	runCmd.Flags().StringSliceP("values", "c", defaultHashes, "Define one or more hashes to compare")
}

// This is the actual command...
func runRun(cmd *cobra.Command, args []string) {
	debug, _ := cmd.Flags().GetBool("debug")
	validLetters, _ := cmd.Flags().GetString("letters")
	hashes, _ := cmd.Flags().GetStringSlice("values")
	shuffleMode, _ := cmd.Flags().GetBool("shuffle")
	exitOnFound, _ := cmd.Flags().GetBool("exitasap")
	asyncMode, _ := cmd.Flags().GetBool("async")

	if debug {
		log.SetLevel(log.DebugLevel)
	} else {
		log.SetLevel(log.InfoLevel)
	}

	rand.Seed(time.Now().Unix())

	// Ensuring that valid letters are sorted. This helps when caching the
	// dictionaries generated
	vlbs := strings.Split(validLetters, "")
	sort.Strings(vlbs)
	validLetters = strings.Join(vlbs, "")

	path := args[0]

	log.Infof("Loading dictionary from %s", path)
	data, err := core.GetDictionary(path)
	if err != nil {
		log.Fatal(err)
	}
	log.Infof("Dictionary loaded with %d entries", len(data))

	// Adds a counter that will print to screen if it is working
	triedPhrases := 0
	lastPhrase := ""

	// two variables to check how many phrases were generated in a given period
	lastAmount := 0
	newAmount := 0

	ticker := time.NewTicker(5 * time.Second)
	go func() {
		for range ticker.C {
			newAmount = triedPhrases - lastAmount

			// Maybe using prometheus next task for SRE?
			log.WithFields(log.Fields{
				"phrased_hashed_count": triedPhrases,
				"last_phrase_hashed":   lastPhrase,
				"goroutine_count":      runtime.NumGoroutine(),
				"phrase_per_second":    newAmount / 5,
			}).Info("Status update")
			lastAmount = triedPhrases
		}
	}()

	phrases := make(chan string)
	var wg sync.WaitGroup
	var wgp sync.WaitGroup

	wgp.Add(1)
	go func() {
		defer wgp.Done()
		// For each phrase spin a goroutine to process the hash
		for p := range phrases {
			h, v := core.IsGoodPhrase(p, hashes...)
			triedPhrases++
			lastPhrase = p

			plog := log.WithFields(log.Fields{
				"phrase": p,
				"hash":   h,
			})
			if h != "" {
				plog.Debug("Hashed")
			}

			if v {
				plog.Warn("🎉")
				// Exit as soon as we have a combination
				if exitOnFound {
					os.Exit(0)
				}
			}
		}
	}()

	// Enable/disable async mode by passing or not a wait group
	var genWg *sync.WaitGroup
	if asyncMode {
		log.Debug("Running in async mode")
		genWg = &wg
	}

	if shuffleMode {
		log.Debug("Running in shuffle mode")
		wg.Add(1)
		core.GetRandomValidPhrase(&data, validLetters, "", phrases, genWg)
	} else {
		log.Debug("Running in sequential mode")
		core.GetSequentialValidPhrase(&data, validLetters, "", phrases, genWg)
	}

	log.Debug("All phrases possible were generated, waiting for the goroutines")
	wg.Wait()
	close(phrases)

	log.Debug("All phrases possible were sent to the channel, waiting for the hahsing")
	wgp.Wait()

	log.Info("Done")
}
