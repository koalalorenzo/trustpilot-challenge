package cmd

import (
	"bufio"
	"fmt"
	"log"
	"os"

	"github.com/spf13/cobra"

	"gitlab.com/koalalorenzo/trustpilot-challenge/core"
)

// filterCmd represents the run command
var filterCmd = &cobra.Command{
	Use:   "filter [path]",
	Args:  cobra.ExactArgs(1),
	Short: "filter words from the dictionary",
	Long:  `This will the words from the dictionary, filter them and populate a new valid dictionary`,
	Run:   filterRun,
}

func init() {
	rootCmd.AddCommand(filterCmd)
	filterCmd.Flags().StringP("letters", "l", "poultryoutwitsants", "specifies the letters to use for the filter")
	filterCmd.Flags().BoolP("easy", "e", false, "use this to have a smaller dictionary that can quickly find the solution (cheating?)")
}

// This is the actual command to filter the dictionary...
func filterRun(cmd *cobra.Command, args []string) {
	validLetters := cmd.Flag("letters").Value.String()
	easyMode := cmd.Flag("easy").Value.String()

	dictF, err := os.Open(args[0])
	if err != nil {
		log.Fatal(err)
	}
	defer dictF.Close()
	s := bufio.NewScanner(dictF)

	for s.Scan() {
		word := s.Text()
		// if easyMode is enabled, then filter less
		if easyMode == "true" {
			if len(word) < 5 && word != "you" && word != "ty" && word != "wu" && word != "not" && word != "lisp" {
				continue
			}
		}

		if _, ok := core.IsValidWordAndLeft(validLetters, word); !ok {
			continue
		}

		fmt.Println(word)
	}

	if err := s.Err(); err != nil {
		log.Fatal(err)
	}
}
