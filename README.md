# Trustpilot code challenge 

This repository contains the code of my implementation to solve the 
[trustpilot challenge](https://followthewhiterabbit.trustpilot.com/cs/step3.html)

Source code available here: [registry.gitlab.com/koalalorenzo/trustpilot-challenge](registry.gitlab.com/koalalorenzo/trustpilot-challenge)

## The Challenge

You've made an important decision. Now, let's get to the matter.

We have a message for you. But we hid it. 
Unless you know the secret phrase, it will remain hidden.

Can you write the algorithm to find it?

Here is a couple of important hints to help you out:
- An anagram of the phrase is: "poultry outwits ants"
- There are three levels of difficulty to try your skills with
- The MD5 hash of the easiest secret phrase is "e4820b45d2277f3844eac66c903e84be"
- The MD5 hash of the more difficult secret phrase is "23170acc097c24edb98fc5488ab033fe"
- The MD5 hash of the hard secret phrase is "665e5bcb0c20062fe8abaaf4628bb154"

Here is a list of english [words](dictionary), it should help you out.

## Requirements

- GNU Make
- `sort` and `uniq` (Available in `busybox`)
- Go (tested with v1.15.2)

## Building and preparing the dictionary

To build the binaries needed, run
```shell
make clean build dictionary
```

This will create one binary in `build/trustpilot-challenge`.

## Running 

### Filtering the dictionary

The first thing to do is to filter the dictionary with words that are actually
useful, this will drastically reduce the amount of cases to check:

```shell
build/trustpilot-challenge filter dictionary.original.txt | sort | uniq >> dictionary
```

If you want more drastic filter, you can pass `--easy` or `-e`, and it will
make sure to have even fewer cases but to include words that are in the 
solution. This is just to make it quicker.

Alternatively you can use:

```shell
make dictionary
```

### Making the combinations

The second step is to make the comination of words. You can run

```shell
build/trustpilot-challenge run ./dictionary
```

If you want to shuffle the dictionary you can pass `--shuffle` or `-s` to the 
binary. This will not change the solution, just randomize the words combination

If you want to use speed up the process but spawn multiple goroutine you can
pass `--async` or `-a` to the binary. **Note**: This uses more memory than the 
default behavior.
