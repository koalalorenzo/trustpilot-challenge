GIT_COMMIT_SHORT ?= $(shell git log -1 --pretty=format:"%h")
VERSION ?= ${GIT_COMMIT_SHORT}

DOCKER_TAG ?= ${VERSION}
DOCKER_IMAGE_FULL ?= registry.gitlab.com/koalalorenzo/trustpilot-challenge:${DOCKER_TAG}
DOCKER_RUN_ARGS ?= 

GOOS ?= 
BUILD_OUT ?= build

ifneq (${GOOS},)
BUILDS_PREFIX := ${GOOS}_
endif
BUILDS_PREFIX ?= 
BINARY_NAME ?= trustpilot-challenge

DICTIONARY_FILE ?= dictionary.original.txt 
DICTIONARY_ARGS ?=

.DEFAULT_GOAL := default

vendor:
	go mod vendor

build:
	CGO_ENABLED=0 go build -a -installsuffix cgo -o ${BUILD_OUT}/${BUILDS_PREFIX}${BINARY_NAME} main.go 

docker_build:
	docker build --no-cache -t ${DOCKER_IMAGE_FULL} .
.PHONY: docker_build

docker_push:
	docker push ${DOCKER_IMAGE_FULL}
.PHONY: docker_push

# I could have used k8s or nomad job, but not sure if that is part of the task
docker_run: dictionary
	docker run -it -v ${PWD}:/data ${DOCKER_RUN_ARGS} ${DOCKER_IMAGE_FULL} run /data/dictionary --shuffle
.PHONY: docker_run

install:
	go install
.PHONY: install

test:
	go test -v ./...
.PHONY: test

clean:
	rm -rf build
	rm -rf vendor
	rm -f dictionary
.PHONY: clean

dictionary: build
	# Removes words that can't be used and create a new dictionary
	build/${BUILDS_PREFIX}${BINARY_NAME} filter ${DICTIONARY_FILE} ${DICTIONARY_ARGS} | sort | uniq >> dictionary

default: clean build dictionary
.PHONY: default