FROM golang:1.15 as base

WORKDIR /app
COPY go.mod go.sum /app/

RUN go mod download
COPY . /app/

# build the app
RUN make clean build -e BUILD_OUT="" -e BINARY_NAME="main"

# Using scratch for super small docker image
FROM scratch
WORKDIR /data
COPY --from=base /main /bin/app

ENTRYPOINT ["/bin/app"]
CMD ["-h"]
