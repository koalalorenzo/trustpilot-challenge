package core

import (
	"bufio"
	"os"
	"sync"
)

var (
	dictionaryFilters      = map[string]*[]string{}
	dictionaryFiltersMutex = sync.RWMutex{}
)

// GetDictionary will get a pre-cleaned dictionary
func GetDictionary(fp string) (data []string, err error) {
	file, err := os.Open(fp)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	s := bufio.NewScanner(file)
	for s.Scan() {
		data = append(data, s.Text())
	}

	if err := s.Err(); err != nil {
		return nil, err
	}

	return
}

// FilterDictWithLetters will filter the Dictionary and provide a new dictionary
// that has only that subset of letters. This is used to get sub-dictionaries
func FilterDictWithLetters(wd *[]string, base string) *[]string {
	dictionaryFiltersMutex.RLock()
	if v, ok := dictionaryFilters[base]; ok {
		dictionaryFiltersMutex.RUnlock()
		return v
	}
	dictionaryFiltersMutex.RUnlock()

	nwd := []string{}
	for _, w := range *wd {
		if _, ok := IsValidWordAndLeft(base, w); !ok {
			continue
		}
		nwd = append(nwd, w)
	}

	dictionaryFiltersMutex.Lock()
	dictionaryFilters[base] = &nwd
	dictionaryFiltersMutex.Unlock()
	return &nwd
}
