package core

import (
	"strings"
)

// IsValidWordAndLeft checks a word is using the letters of the unusedLetters and
// returns the new unused letters
func IsValidWordAndLeft(unusedLetters, word string) (nUnused string, _ bool) {
	// if the word has more letters than the one we are looking for, skip
	if len(word) > len(unusedLetters) {
		return unusedLetters, false
	}

	nUnused = unusedLetters
	for _, nl := range word {
		if strings.Contains(nUnused, string(nl)) {
			nUnused = strings.Replace(nUnused, string(nl), "", 1)
		} else {
			return unusedLetters, false
		}
	}
	return nUnused, true
}
