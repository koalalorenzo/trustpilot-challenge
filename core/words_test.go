package core

import (
	"math/rand"
	"strings"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestIsValidWordAndLeftValid(t *testing.T) {
	rand.Seed(time.Now().Unix())

	un, valid := IsValidWordAndLeft("worker", "rework")
	assert.True(t, valid)
	assert.Empty(t, un)
}

func TestIsValidWordAndLeftInvalid(t *testing.T) {
	rand.Seed(time.Now().Unix())

	un, valid := IsValidWordAndLeft("wurker", "rework")
	assert.False(t, valid)
	assert.NotEmpty(t, un)
}

func TestIsValidWordAndLeftMultipleLetters(t *testing.T) {
	rand.Seed(time.Now().Unix())

	un, valid := IsValidWordAndLeft("wwwaw", "wawww")
	assert.True(t, valid)
	assert.Empty(t, un)
}

func TestIsValidPartialLetters(t *testing.T) {
	rand.Seed(time.Now().Unix())

	un, valid := IsValidWordAndLeft("superworkers", "rework")
	assert.True(t, valid)
	for _, r := range "super" {
		assert.Contains(t, un, string(r))
	}

	assert.Equal(t, 1, strings.Count(un, "r"))
	assert.Equal(t, 2, strings.Count(un, "s"))
	assert.NotEmpty(t, un)
}
