package core

import (
	"math/rand"
	"sync"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestGetSequentialValidPhrase(t *testing.T) {
	results := make(chan string)
	go GetSequentialValidPhrase(wdict, letters, "", results, nil)
	a := <-results
	assert.Contains(t, a, "a")
	assert.Contains(t, a, "cluster")
	assert.Contains(t, a, "of")
	assert.Contains(t, a, "ec2")
	assert.Contains(t, a, "machines")
}

func TestGetRandomSubdictValidPhrase(t *testing.T) {
	results := make(chan string)
	rand.Seed(time.Now().Unix())
	go GetRandomValidPhrase(wdict, letters, "", results, nil)
	a := <-results
	assert.Contains(t, a, "a")
	assert.Contains(t, a, "cluster")
	assert.Contains(t, a, "of")
	assert.Contains(t, a, "ec2")
	assert.Contains(t, a, "machines")
}

func TestGetSequentialValidPhraseGo(t *testing.T) {
	results := make(chan string)
	var wg sync.WaitGroup
	wg.Add(1)
	go GetSequentialValidPhrase(wdict, letters, "", results, &wg)
	a := <-results
	assert.Contains(t, a, "a")
	assert.Contains(t, a, "cluster")
	assert.Contains(t, a, "of")
	assert.Contains(t, a, "ec2")
	assert.Contains(t, a, "machines")
}

func TestGetRandomSubdictValidPhraseGo(t *testing.T) {
	results := make(chan string)
	rand.Seed(time.Now().Unix())
	var wg sync.WaitGroup
	wg.Add(1)
	go GetRandomValidPhrase(wdict, letters, "", results, &wg)
	a := <-results
	assert.Contains(t, a, "a")
	assert.Contains(t, a, "cluster")
	assert.Contains(t, a, "of")
	assert.Contains(t, a, "ec2")
	assert.Contains(t, a, "machines")
}
