package core

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

var (
	wdict   = &[]string{"a", "cluster", "kubernetes", "lambda", "of", "ec2", "machines", "terraform"}
	letters = "aclusterofec2machines"
)

func TestFilterDictWithLettersValid(t *testing.T) {
	a := FilterDictWithLetters(wdict, letters)
	assert.Contains(t, *a, "a")
	assert.Contains(t, *a, "cluster")
	assert.Contains(t, *a, "of")
	assert.Contains(t, *a, "ec2")
	assert.Contains(t, *a, "machines")
	assert.NotContains(t, *a, "lambda")
	assert.NotContains(t, *a, "terraform")
}
