package core

import (
	"crypto/md5"
	"fmt"
	"io"
)

func GetHash(x string) string {
	h := md5.New()
	io.WriteString(h, x)
	return fmt.Sprintf("%x", h.Sum(nil))
}

// IsGoodPhrase generates the hash of specific strings and checks if it is valid
func IsGoodPhrase(x string, solutions ...string) (hash string, valid bool) {
	valid = false
	hash = ""

	hash = GetHash(x)
	// should we use a map for quicker lookup? we just have 3 strings to compare
	for _, v := range solutions {
		if v == hash {
			valid = true
		}
	}

	return
}
