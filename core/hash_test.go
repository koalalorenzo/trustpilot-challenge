package core

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

var (
	hashes = []string{
		"e4820b45d2277f3844eac66c903e84be",
		"23170acc097c24edb98fc5488ab033fe",
		"665e5bcb0c20062fe8abaaf4628bb154",
	}
)

func TestIsGoodPhraseValid(t *testing.T) {
	h0, b0 := IsGoodPhrase("printout stout yawls", hashes...)
	assert.Equal(t, "e4820b45d2277f3844eac66c903e84be", h0)
	assert.True(t, b0)

	h1, b1 := IsGoodPhrase("ty outlaws printouts", hashes...)
	assert.Equal(t, "23170acc097c24edb98fc5488ab033fe", h1)
	assert.True(t, b1)

	h2, b2 := IsGoodPhrase("wu lisp not statutory", hashes...)
	assert.Equal(t, "665e5bcb0c20062fe8abaaf4628bb154", h2)
	assert.True(t, b2)
}

func TestIsGoodPhraseInvalid(t *testing.T) {
	_, b0 := IsGoodPhrase("trustpilot wants you", hashes...)
	assert.False(t, b0)

	_, b1 := IsGoodPhrase("wut unrol tipsy toast", hashes...)
	assert.False(t, b1)
}
