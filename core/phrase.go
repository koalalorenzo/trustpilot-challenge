package core

import (
	"math/rand"
	"strings"
	"sync"
)

// GetSequentialValidPhrase is searches sequentially starting from the first
// in the dictionary and then extract a subdictionary based on the available letters.
func GetSequentialValidPhrase(wdict *[]string, letters, prefix string, ch chan<- string, wg *sync.WaitGroup) {
	if wg != nil {
		defer wg.Done()
	}

	for _, word := range *wdict {
		// Get unused letters by this word
		ul, _ := IsValidWordAndLeft(letters, word)

		// Combine the word with the previous words / prefix
		sentence := word
		if len(prefix) > 0 {
			sentence = strings.Join([]string{prefix, word}, " ")
		}

		// if we have no more letters used then we have found a combo!
		// we will then send it to the channel
		if len(ul) == 0 {
			ch <- sentence
			continue
		}

		// Get a new dictionary with acceptable words
		ndict := FilterDictWithLetters(wdict, ul)

		// if we have some words and letters left
		if len(*ndict) > 0 && len(ul) > 0 {
			if wg != nil {
				wg.Add(1)
				go GetSequentialValidPhrase(ndict, ul, sentence, ch, wg)
			} else {
				GetSequentialValidPhrase(ndict, ul, sentence, ch, nil)
			}
		}
	}
}

// GetRandomValidPhrase is like GetSequentialValidPhrase but takes random
// order of the element in the dictionaries.
func GetRandomValidPhrase(wdict *[]string, letters, prefix string, ch chan string, wg *sync.WaitGroup) {
	if wg != nil {
		defer wg.Done()
	}

	wdictSize := len(*wdict)
	rand.Shuffle(wdictSize, func(i, j int) {
		(*wdict)[i], (*wdict)[j] = (*wdict)[j], (*wdict)[i]
	})
	// I am not using a random index as this might cause the same word to be
	// picked twice... even if it would be less effective
	// for i := 0; i < wdictSize; i++ {
	// 	randomIndex := rand.Intn(wdictSize)
	// 	word := (*wdict)[randomIndex]

	for _, word := range *wdict {

		// Get unused letters by this word
		ul, _ := IsValidWordAndLeft(letters, word)

		// Combine the word with the previous words / prefix
		sentence := word
		if len(prefix) > 0 {
			sentence = strings.Join([]string{prefix, word}, " ")
		}

		// if we have no more letters used then we have found a combo!
		// we will then send it to the channel
		if len(ul) == 0 {
			ch <- sentence
			continue
		}

		// Get a new dictionary with acceptable words
		ndict := FilterDictWithLetters(wdict, ul)

		// if we have some words and letters left
		if len(*ndict) > 0 && len(ul) > 0 {
			if wg != nil {
				wg.Add(1)
				go GetRandomValidPhrase(ndict, ul, sentence, ch, wg)
			} else {
				GetRandomValidPhrase(ndict, ul, sentence, ch, nil)
			}
		}
	}
}
